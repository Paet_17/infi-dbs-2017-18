import configparser
import os


class ConfigManager:
    path = 'config.ini'
    config_file = None
    config = None

    def __init__(self):
        if not (os.path.isfile(self.path)):
            self.config = configparser.ConfigParser()
            self.config['DATABASE'] = {'DB_Name': 'fuho.db',
                                       'Sel_Table': 'ID6_DATA'}
            self.config['Min_Max'] = {'Tab1' : 'ID4_DATA',
                                      'Tab2' : 'ID5_DATA',
                                      'Tab3' : 'ID6_DATA',
                                      'Tab4' : 'ID8_DATA',
                                      'Tab5' : 'ID9_DATA'}
            with open(self.path, 'w') as configfile:
                self.config.write(configfile)

    def get_DB_name(self):
        self.config = configparser.ConfigParser()
        self.config.read(self.path)
        database_name = self.config['DATABASE']['DB_Name']
        return database_name

    def get_sel_table(self):
        self.config = configparser.ConfigParser()
        self.config.read(self.path)
        sel_table = self.config['DATABASE']['Sel_Table']
        return sel_table

    def get_mima_tables(self):
        self.config = configparser.ConfigParser()
        self.config.read(self.path)
        mima = []
        for i in range(1,6):
            mima.append(self.config['Min_Max']['Tab' + str(i)])
        return mima

    def set_sel_table(self, table_name):
        self.config = configparser.ConfigParser()
        self.config.read(self.path)
        self.config['DATABASE']['Sel_Table'] = table_name
        #self.config.set('DATABASE', 'Sel_Table', table_name)
        with open(self.path, 'w') as configfile:
            self.config.write(configfile)

    def set_mima_tab(self,i, tab):
        self.config = configparser.ConfigParser()
        self.config.read(self.path)
        self.config['Min_Max']['Tab' + str(i)] = tab
        with open(self.path, 'w') as configfile:
            self.config.write(configfile)


