import matplotlib.pyplot as plt
import numpy as np

from DBManager import DBManager

db_m = DBManager()
avges = db_m.get_averages()
#x_curr = []
x_val1 = []
x_val2 = []
avges_k = avges.keys()
avges_val = avges.values()
y = []
y.extend(range(1,14))

for val in avges_val:
    #x_curr.append((val[0]/100))
    x_val1.append((val[1]/100))
    x_val2.append((val[2]/100))


n = 13
ind = np.arange(n) #x location for values
width = 0.4
fig, ax = plt.subplots()


#current = ax.bar(ind, x_curr, width, color = 'r' ,label='current')

value1 = ax.bar(ind, x_val1, width, color = 'g', label='temperature')

value2 = ax.bar(ind-width, x_val2, width, color = 'c', label='humidity')

ax.set_ylabel('temperature[°C] / humidity[%]')
#ax.set_xlabel('Months from a year ago to now')
ax.set_xticks(ind + width / 2)
ax.set_xticklabels(avges_k)
x_curr_max = int(max(x_val1))
#ax.set_yticks(range(0,x_curr_max+2))
#ticksy = range(0,x_curr_max+2, 2)
#ax.set_yticklabels(ticksy)


def autolabels(bar_ch):
    for b in bar_ch:
        height = b.get_height()
        ax.text(b.get_x() + b.get_width()/2.0, 1.05*height,
                '%d' % int(height),
                ha = 'center', va='bottom')


autolabels(value1)
autolabels(value2)

#table
row = db_m.get_mima_names()
rows = []
for name in row:
    rows.append(name[0] + ' (' + name[1] + ')')

columns = ['Date', 'max t', 'Date', 'max h', 'Date', 'min t',
           'Date', 'min h']

max = db_m.select_max()
min = db_m.select_min()
data = []

for da in max:
    d = []
    d.extend([da[1][0][0], da[1][0][1], da[1][1][0], da[1][1][1]])
    data.append(d)

i = 0
for da in min:
    d = []
    d.extend([da[1][0][0], da[1][0][1], da[1][1][0], da[1][1][1]])
    data[i].extend(d)
    i += 1



#add table
mima_table = plt.table(cellText = data, rowLabels = rows, colLabels = columns,
                     cellLoc='center', loc = 'lower bottom', bbox=[0.5,-0.5,0.5,0.4])
mima_table.auto_set_font_size(False)
mima_table.set_fontsize(10)
#mima_table.auto_set_column_width(True)
#mima_table.scale(1.5,1.5)

#tryna fix the table width
cellDict =  mima_table.get_celld()
date_w = 4
val_w = 2
for i in range(0,6):
    cellDict[(i,0)].set_width(date_w)
    cellDict[(i, 2)].set_width(date_w)
    cellDict[(i, 4)].set_width(date_w)
    cellDict[(i, 6)].set_width(date_w)

    cellDict[(i, 1)].set_width(val_w)
    cellDict[(i, 3)].set_width(val_w)
    cellDict[(i, 5)].set_width(val_w)
    cellDict[(i, 7)].set_width(val_w
                               )
#make room for table
plt.subplots_adjust( bottom=0.3)

#add text
plt.gcf().text(0.15,0.1, 'Min/Max of Stations:', fontsize=14)

#final configurations
title = db_m.get_sel_name()
title = 'Visualisation of the place ' + title[0] + '(' + title[1] + ')'
plt.suptitle(title, fontweight='bold')
ax.legend()
plt.show()