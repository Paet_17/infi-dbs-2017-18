from DBManager import DBManager
import os

go_on = True
db_m = DBManager()


def menue():
    print("Welcome to the CLI of the DataVisualiser!\n")
    print("Please select an action:")
    print("s\tSelect all from selected table.")
    print("mi\tSelect min value from Table.")
    print("ma\tSelect max value from Table.")
    print("c\tTo change table.")
    print("cs\tTo change minmax table.")
    print("µ\tTo get monthly average.")
    print("v\tTo visualise.")
    print("x\tTo exit Program.")



while go_on:
    menue()
    chosen = input()
    if chosen is 's':
        print()
        print(db_m.select_all())
        input("Press enter to continue...")
        print()
    elif chosen is 'x':
        exit(0)
    elif chosen is 'c':
        print("New Table:\n")
        table = input()
        db_m.set_sel_table(table)
    elif chosen == 'mi':
        print("Min value of selected Table is:\n")
        print(db_m.select_min())
        input("Press enter to continue...")
        print()
    elif chosen == 'ma':
        print("Max value of selected file is :\n")
        print(db_m.select_max())
        input("Press enter to continue...")
        print()
    elif chosen == 'µ':
        print("Monthly values:\n")
        dict = db_m.get_averages()
        for month in dict:
            print(month, dict[month])
        print()
    elif chosen == 'v':
        print("Table gets visualised and should pop up now ... \n")
        os.system('Visualise.py')
    elif chosen == 'cs':
        print("Which table should be replaced? (1-5)")
        print(db_m.get_mima_names())
        i = input()
        print("With which table should it be replaced?")
        tab = input()
        db_m.set_mima_tab(i,tab)
        input("Press enter to continue...")
        print()

    else:
        print("Invalid input! Please try again.\n")
        input("Press enter to continue...")
