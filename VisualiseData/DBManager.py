import sqlite3
from Config import ConfigManager
import datetime


class DBManager:

    __conn = None
    __db_name = None
    __sel_table = None
    __curr_date = datetime.datetime

    cf_m = ConfigManager()

    def __init__(self):
        self.__db_name = self.cf_m.get_DB_name()
        self.__sel_table = self.cf_m.get_sel_table()

    def select_all(self):
        self.__open_conn()
        c = self.__conn.cursor()
        c.execute('select * from ' + self.__sel_table)
        s = c.fetchall()
        self.__close_conn()
        return s

    def select_max(self):
        s = self.__select_min_max('max')
        return s

    def select_min(self):
        s = self.__select_min_max('min')
        return s

    def __select_min_max(self,min_max):
        self.__open_conn()
        c = self.__conn.cursor()
        timestamp = self.__get_timest_1y_ago()
        timestamp = str(timestamp)
        s = []
        for tab in self.get_mima_tables():
            ss = []
            c.execute('select ' + min_max + '(value1) from ' + tab + ' where datecolumn >= ' + timestamp)
            mi_ma = c.fetchone()
            mi_ma = mi_ma[0]
            if mi_ma is not None:
                c.execute('select datecolumn from ' + tab + ' where value1 = ' + str(mi_ma))
                time = c.fetchone()
                time = time[0]
                time = datetime.datetime.fromtimestamp(time).strftime('%Y-%m-%d')
                mi_ma = '%1.1f' % (mi_ma / 100)
            else:
                time = 'N/A'
            sss = [time,mi_ma]
            ss.append(sss)
            c.execute('select ' + min_max + '(value2) from ' + tab + ' where datecolumn >= ' + timestamp)
            mi_ma = c.fetchone()
            mi_ma = mi_ma[0]
            if mi_ma is not None:
                c.execute('select datecolumn from ' + tab + ' where value2 = ' + str(mi_ma))
                time = c.fetchone()
                time = time[0]
                time = datetime.datetime.fromtimestamp(time).strftime('%Y-%m-%d')
                mi_ma = '%1.1f' % (mi_ma / 100)
            else:
                time = 'N/A'
            sss = [time,mi_ma]
            ss.append(sss)

            s.append([tab,ss])
        self.__close_conn()
        return s

    def get_averages(self):
        year_ago = self.__get_timest_1y_ago()
        date = datetime.datetime.fromtimestamp( int (year_ago))
        month_name = date.strftime("%B")
        month_end = self.__get_last_day_of_mont(date)
        month_start = date
        dict_avg = {}
        for i in range(0,13):
            month_end_ts = (int) (month_end.timestamp())
            month_start_ts = (int) (month_start.timestamp())
            avges = self.__get_avges_db(month_start_ts, month_end_ts)
            dict_avg[month_name] = avges

            month_start = self.__get_first_day_next_month(month_start)
            month_end = self.__get_last_day_of_mont(month_end)
            month_name = month_start.strftime("%B")
            if i == 11:
                year = str(datetime.datetime.now().year)
                month_name +=  year[2:4]
        return dict_avg

    def __get_avges_db(self, start, end):
        avg = None
        avg_curr = None
        avg_val1 = None
        avg_val2 = None
        avg_curr = self.__get_avg_db(start,end,'current')
        avg_val1 = self.__get_avg_db(start,end, 'value1')
        avg_val2 = self.__get_avg_db(start,end, 'value2')
        avg = [avg_curr, avg_val1, avg_val2]
        return avg

    def __get_avg_db(self,start, end, col):
        self.__open_conn()
        c = self.__conn.cursor()
        start = str(start)
        end = str(end)
        avg = None
        c.execute('select sum(' + col + ') from ' + self.__sel_table
                  + ' where datecolumn '
                  + ' between ' + start + ' and ' + end)
        sum = c.fetchone()
        c.execute('select count(*) from ' + self.__sel_table
                  + ' where datecolumn '
                  + ' between ' + start + ' and ' + end)
        num_val = c.fetchone()
        self.__close_conn()
        try:
            avg = sum[0] / num_val[0]
        except:
            avg = 0;
        return avg


    def __get_last_day_of_mont(self, date):
        if date.month == 12:
            return date.replace(day=31)
        return date.replace(month=date.month+1, day=1) - datetime.timedelta(days=1)

    def __get_first_day_next_month(self,date):
        if date.month == 12:
            date = date.replace(month=1, day=1)
        else:
            date = date.replace(month=date.month+1, day=1)
        return date

    def set_sel_table(self, table):
        self.__sel_table = table
        self.cf_m.set_sel_table(table)

    def set_mima_tab(self, i, tab):
        self.cf_m.set_mima_tab(i,tab)

    def __get_timest_1y_ago(self):
        date = datetime.datetime.now()
        date = date - datetime.timedelta(days=365)
        date = (int)(date.timestamp())
        return date

    def get_sel_name(self):
        tab = self.__sel_table
        return self.__get_tab_name(tab)

    def get_mima_names(self):
        names = []
        for tab in self.get_mima_tables():
            name = self.__get_tab_name(tab)
            names.append(name)
        return names


    def __get_tab_name(self, tab):
        self.__open_conn()
        c = self.__conn.cursor()
        if len(tab) == 8:
            id = tab[2]
        elif len(tab) == 9:
            id = tab[2:4]
        c.execute('select name from id_descriptions where id = ' + id)
        name = c.fetchone()
        c.execute('select description from id_descriptions where id = ' + id)
        des = c.fetchone()
        self.__close_conn()
        return [name[0],des[0]]

    def __open_conn(self):
        self.__conn = sqlite3.connect(self.__db_name)

    def __close_conn(self):
        self.__conn.close()

    def get_mima_tables(self):
        return self.cf_m.get_mima_tables()

test = DBManager()
max = test.select_max()
min = test.select_min()

print(max)
print(min)


