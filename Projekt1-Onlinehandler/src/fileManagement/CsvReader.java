package fileManagement;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;

import sqliteManagement.DBManager;

public class CsvReader {
	
	String pfad;
	BufferedReader br = null;
	String line = "";
	String csvSplitBy = ";";
	DBManager dbm;
	String[] read;
	
	
	public CsvReader()
	{
		
	}
	
	public void readArtikel(String pfad) throws SQLException
	{
		
		read = null;
		try{
			br = new BufferedReader(new FileReader(pfad));
			while ((line = br.readLine()) != null){
				read = line.split(csvSplitBy);
				insertArtikle(read);
				
				
				
			}
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
	}
   }
		
		

}
	
	public void readBestellungen(String pfad) throws SQLException
	{
		
		read = null;
		try{
			br = new BufferedReader(new FileReader(pfad));
			while ((line = br.readLine()) != null){
				read = line.split(csvSplitBy);
				insertBestellungen(read);
				
				
				
			}
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
	}
   }
		
		

}
	
	public void readKunden(String pfad) throws SQLException
	{
		
		read = null;
		try{
			br = new BufferedReader(new FileReader(pfad));
			while ((line = br.readLine()) != null){
				read = line.split(csvSplitBy);
				insertKunden(read);
				
				
				
			}
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
	}
   }
		
		

}

	private void insertArtikle(String[] read) throws SQLException {
		//Null Objekte beruecksichtigen
		if(read.length == 5)
		{
			read[5] = null;
		}
		
		//Strings in die richtigen Werte umwandeln
		String sartNr = read[0];
		int artNr = Integer.parseInt(sartNr);
		
		String sPreis = read[1];
		double preis = Double.parseDouble(sPreis);
		
		String sBestand = read[3];
		int bestand = Integer.parseInt(sBestand);
		
		dbm = new DBManager();
		
		try {
			dbm.insertArtikel(artNr, preis, read[2], bestand, read[4], read[5]);
		} catch (Exception e) {
			System.out.println("Datensatz mit dem Primaerschluessel "
					+ read[0] +" wurde bereits eingelesen!");
			
		}
	}
	
	private void insertBestellungen(String[] read) throws SQLException {
		
		
		//Strings in die richtigen Werte umwandeln
		String sartNr = read[0];
		int artNr = Integer.parseInt(sartNr);
		
		int bestellNr = Integer.parseInt(read[1]);
		
		int stk = Integer.parseInt(read[2]);
		
		dbm = new DBManager();
		
		try {
			dbm.insertBestellung(artNr, bestellNr, stk, read[3], read[4]);
		} catch (Exception e) {
			System.out.println("Datensatz mit dem Primaerschluessel "
					+ read[1] +" wurde bereits eingelesen!");
		}
	}
	
	private void insertKunden(String[] read) throws SQLException {
		
		String stiege;
		String postfach;
		//Null Objekte beruecksichtigen
		if(read.length <= 11)
		{
			stiege = null;
		}else{
			stiege = read[11];
		}
		
		if(read.length <= 10)
		{
			postfach = null;
		}else{
			postfach = read[10];
		}
		
		//Strings in die richtigen Werte umwandeln
		int adrID = Integer.parseInt(read[4]);
		
		dbm = new DBManager();
		
		try {
			dbm.insertKunde(read[0], read[1], read[2], read[3], adrID, read[5],	read[6], read[7], read[8], read[9], postfach, stiege);
		} catch (Exception e) {
			System.out.println("Datensatz mit dem Primaerschluessel "
					+ read[0] +" wurde bereits eingelesen!");
		}
	}
	
	
	
	
	
}
