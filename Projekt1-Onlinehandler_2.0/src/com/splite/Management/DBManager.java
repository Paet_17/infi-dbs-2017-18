package com.splite.Management;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class DBManager {
	
	
	Connection c = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	String comm = "";
	
	public DBManager() throws SQLException
	{
		
		OpenConnection();
		c.close();
		
		createDatabase();
	}
	
	public void OpenConnection() throws SQLException 
	{
		
		try{
			Class.forName("org.sqlite.JDBC");
			c=DriverManager.getConnection(("jdbc:sqlite:C:\\Users\\pakeu\\Workspaces\\infi-dbs-2017-18\\Projekt1-Onlinehandler_2.0\\Onlinehandler.db"));
			c.setAutoCommit(false);
			
		}
		catch (Exception e)
		{
			System.err.println(e.getClass().getName() + ": " + e.getMessage() );
			System.out.println("Verbindung zur Datenbank ist nicht moeglich!");
			System.exit(0);
		}
		comm ="PRAGMA foreign_keys = 1;";
		ps = c.prepareStatement(comm);
		ps.executeUpdate();
	}
//TODO fix foreign keys
	public void createDatabase() throws SQLException
	{
		OpenConnection();
		
		comm = "create table if not exists Artikel("
				+ "Artikelnummer integer primary key autoincrement,"
				+ "Preis real not null,"
				+ "Bezeichnung text not null,"
				+ "Lagerbestand integer not null,"
				+ "Bild blob null,"
				+ "Kategoriename text not null,"
				+ "Beschreibung text null,"
				+ "Foreign Key (Kategoriename) references Artikelkategorie(Kategoriename));";
		ps = c.prepareStatement(comm);
		ps.executeUpdate();
		comm = "create table if not exists Artikelkategorie ( "
				+ " Kategoriename text primary key "
								+ ");";
		ps = c.prepareStatement(comm);
		ps.executeUpdate();
		comm = "create table if not exists wirdBestellt("
				+ "Artikelnummer integer,"
				+ "Bestellungsnummer integer,"
				+ "Stueckzahl not null,"
				+ "FOREIGN KEY( Artikelnummer ) REFERENCES  wirdBestellt(Artikelnummer) , "
				+ " FOREIGN KEY( Bestellungsnummer ) REFERENCES Bestellung(Bestellungsnummer) , "
				+ "Primary Key (Artikelnummer, Bestellungsnummer)"
				+ ");";
		ps = c.prepareStatement(comm);
		ps.executeUpdate();
		comm = "create table if not exists Bestellung("
				+ "Bestellungsnummer integer primary key autoincrement,"
				+ "Email text,"
				+ "Datum text not null,"
				+ "Foreign Key( Email) references Kunde(Email)"
				+ ");";
		ps = c.prepareStatement(comm);
		ps.executeUpdate();
		comm = "create table if not exists Kunde("
				+ "Email text primary key,"
				+ "Vorname text not null,"
				+ "Nachname text not null,"
				+ "Geburtsdatum text not null,"
				+ "Adressen_ID int not null,"
				+ "Profilbild blob null,"
				+ "Foreign Key (Adressen_ID) references Adresse(Adressen_ID));";
		ps = c.prepareStatement(comm);
		ps.executeUpdate();
		comm="create table if not exists Adresse("
				+ "Adressen_ID integer primary key autoincrement,"
				+ "Land text not null,"
				+ "Ort text not null,"
				+ "Strasse text not null,"
				+ "Hausnummer text not null,"
				+ "Postleizahl text not null,"
				+ "Postfach text null,"
				+ "Stiege text null);";
		ps = c.prepareStatement(comm);
		ps.executeUpdate();
		c.commit();
		ps.close();
		c.close();
		
	}

	//IF the table has 2 columns with integer, integer
	
	public void insertArtikel(int artikelNr, double preis, String bez, int lagbest, String kategorie, String beschreibung) throws SQLException
	{
		OpenConnection();
		
		ps = c.prepareStatement("INSERT INTO Artikel ( Artikelnummer, Preis, Bezeichnung, Lagerbestand,  Bild, Kategoriename, Beschreibung)VALUES(?,?,?,?,?,?,?)");
	      ps.setInt(1, artikelNr);
	      ps.setDouble(2, preis);
	      ps.setString(3, bez);
	      ps.setInt(4, lagbest);
	      ps.setString(5, null);
	      ps.setString(6, kategorie);
	      ps.setString(7, beschreibung);
	      
	      ps.executeUpdate();
	      
	      ps= c.prepareStatement("INSERT INTO Artikelkategorie ( Kategoriename ) VALUES (?)");
	      ps.setString(1, kategorie);
	      ps.executeUpdate();
	      
	      ps.close();
	      c.commit();
	      c.close();
	}
	
	
	
	public void insertBestellung(int artNr, int bestellNr, int stk, String email, String datum) throws SQLException
	{
		OpenConnection();
		
		ps = c.prepareStatement("INSERT INTO wirdBestellt(Artikelnummer, Bestellungsnummer, Stueckzahl) VALUES(?,?,?)");
		ps.setInt(1, artNr);
		ps.setInt(2, bestellNr);
		ps.setInt(3, stk);
		
		ps.executeUpdate();
		
		ps=c.prepareStatement("INSERT INTO Bestellung(Bestellungsnummer, Email, Datum) VALUES (?,?,?)");
		ps.setInt(1, bestellNr);
		ps.setString(2, email);
		ps.setString(3, datum);
		
		ps.executeUpdate();
		
		ps.close();
		c.commit();
		c.close();
	}
	
	public int insertBestellung(int artNr,  int stk, String email, String datum) throws SQLException
	{
		OpenConnection();
		
		
		
		ps=c.prepareStatement("INSERT INTO Bestellung( Email, Datum) VALUES (?,?)");
		ps.setString(1, email);
		ps.setString(2, datum);
		
		ps.executeUpdate();
		
		int bestellNr = -1;
		
		comm = "select max(Bestellungsnummer) from Bestellung;";
		ps = c.prepareStatement(comm);
		rs = ps.executeQuery();
		bestellNr = rs.getInt(1);
		rs.close();
		
		ps = c.prepareStatement("INSERT INTO wirdBestellt(Artikelnummer, Bestellungsnummer, Stueckzahl) VALUES(?,?,?)");
		ps.setInt(1, artNr);
		ps.setInt(2, bestellNr);
		ps.setInt(3, stk);
		
		ps.executeUpdate();
		
		
		
		ps.close();
		c.commit();
		c.close();
		return bestellNr;
		
		
	}
	
	public void insertKunde(String email, String vName, String Nname, String date, int adrID, String land, String ort, String strasse, String nr, String plz, String postfach, String stiege) throws SQLException
	{
		OpenConnection();
		
		System.out.println("test");
		
		ps = c.prepareStatement("INSERT INTO Kunde(Email, Vorname, Nachname, Geburtsdatum, Adressen_ID, Profilbild) VALUES(?,?,?,?,?,?)");
		ps.setString(1, email);
		ps.setString(2, vName);
		ps.setString(3, Nname);
		ps.setString(4, date);
		ps.setInt(5, adrID);
		ps.setString(6, null);
		
		ps.executeUpdate();
		
		ps = c.prepareStatement("INSERT INTO Adresse(Adressen_ID, Land, Ort, Strasse, Hausnummer, Postleizahl, Postfach, Stiege) VALUES(?,?,?,?,?,?,?,?)");
		ps.setInt(1, adrID);
		ps.setString(2, land);
		ps.setString(3, ort);
		ps.setString(4, strasse);
		ps.setString(5, nr);
		ps.setString(6, plz);
		ps.setString(7, postfach);
		ps.setString(8, stiege);
		
		ps.executeUpdate();
		ps.close();
		c.commit();
		c.close();
		
	}
	
	public void insertKunde(String email, String vName, String Nname, String date, String land, String ort, String strasse, String nr, String plz, String postfach, String stiege) throws SQLException
	{
		OpenConnection();
		
		
		
		ps = c.prepareStatement("INSERT INTO Adresse( Land, Ort, Strasse, Hausnummer, Postleizahl, Postfach, Stiege) VALUES(?,?,?,?,?,?,?)");
		
		ps.setString(1, land);
		ps.setString(2, ort);
		ps.setString(3, strasse);
		ps.setString(4, nr);
		ps.setString(5, plz);
		ps.setString(6, postfach);
		ps.setString(7, stiege);
		
		ps.executeUpdate();
		
		int adrID = -1;
		

		comm = "select max(Adressen_ID) from Adresse;";
		ps = c.prepareStatement(comm);
		rs = ps.executeQuery();
		adrID = rs.getInt(1);
		rs.close();
		
		
		ps = c.prepareStatement("INSERT INTO Kunde(Email, Vorname, Nachname, Geburtsdatum, Adressen_ID, Profilbild) VALUES(?,?,?,?,?,?)");
		ps.setString(1, email);
		ps.setString(2, vName);
		ps.setString(3, Nname);
		ps.setString(4, date);
		ps.setInt(5, adrID);
		ps.setString(6, null);
		
		ps.executeUpdate();
		
		
		ps.close();
		c.commit();
		c.close();
		
		
		
	}
	
	

	
	public ArrayList<String> artikelSuchen(String artikelName, String kategorie) throws SQLException
	{
		OpenConnection();
		
		ArrayList<String> artikel = new ArrayList<String>();
		
		if((artikelName.equals("")) && (kategorie.equals("Alle"))) {
			comm = "select * from Artikel join Artikelkategorie using (Kategoriename);";
		}else if(artikelName.equals("")) {
			
			comm = "select * from Artikel join Artikelkategorie using (Kategoriename)"
					+ " where Kategoriename like '%"  + kategorie + "%';";
		}else {
			comm = "select * from Artikel  "
					+ "join Artikelkategorie using (Kategoriename)"
					+ " where (Kategoriename like '%"  + kategorie + "%') and"
							+ "(Bezeichnung like '%" + artikelName +"%');";
		}
		ps=c.prepareStatement(comm);
		
		rs = ps.executeQuery();
		
		while(rs.next()) {
			
			artikel.add(rs.getString(1) + "\t" + rs.getString(2) + "\t" +rs.getString(3)+ "\t" +rs.getString(4)
			+ "\t" +rs.getString(6) + "\t" +rs.getString(7) );
			
		}
		
		if(artikel.isEmpty())
		{
			artikel.add("Die Suche ergab leider keine Treffer!");
		}
		
		
		ps.close();
		rs.close();
		c.close();
		
		return artikel;
	}
	
	public boolean redundanzTest(String primaryK, String table, String colName) throws SQLException {
		boolean vorhanden = false;
		OpenConnection();
		comm = "select * from " + table + " where " + colName + " = '" + primaryK + "';";
		ps = c.prepareStatement(comm);
		rs = ps.executeQuery();
		
		while(rs.next()){
			if(!((rs.getString(1).equals(null)))) return true;
		}
		
		c.close();
		return vorhanden;
	}
	

	
	public boolean loeschen(String tab, int zeile, String idName) throws SQLException
	{
		OpenConnection();
		String comm = "DELETE FROM " + tab + " WHERE " + idName +  "=" + zeile;
		PreparedStatement ps = c.prepareStatement(comm);
		ps.executeUpdate();
		c.commit();
		ps.close();
		c.close();
		return true;
		
	}
	
	public void closeConn() throws SQLException
	{
		c.close();
	}

}
