package com.jsp.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.splite.Management.DBManager;

/**
 * Servlet implementation class Home
 */
@WebServlet("/Home")
public class Home extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String suche;
	private String kategorie;
	
	
	DBManager dbm;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Home() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		
		
			
	
		
		PrintWriter out = response.getWriter();
		
		out.println("<html> <head> <title>Murat-3xpr3ss</title>"
				+ " <link rel=stylesheet href='stylesheet.css' />"
				+ "</head><header><h1>Murat-3xpr3ss</h1></header><body>");
		
		out.println("<br/><br/>");
		
		out.println("<form action='Home'>"
				+ "Suche: <input type=\"text\" name =\"suche\"/>"
				+ " In Kategorie: <select name='kategorie'> \n"
				+ "<option>Alle</option> \n"
				+ "<option>Freizeit</option> \n"
				+ "<option>Haushalt</option> \n"
				+ "<option>Technik</option> \n"
				+ "<option>Kueche</option> \n"
				+ "</select>"
				+ "<br/>"
				+ "<input type=\"submit\" value=\"Suchen\"/>"
				+ "<br/>"
				+ "<h4>Murats Angebote mit Bestpreisgarantie:</h4>"
				+ "</form>");
		
		
		suche = request.getParameter("suche");
		kategorie = request.getParameter("kategorie");
		
		ArrayList<String> artikel = new ArrayList<String>();
		ArrayList<String> artikelIDs = new ArrayList<String>();
		
		if(suche == null) suche = "";
		if(kategorie == null) kategorie = "Alle";
		
		try {
			dbm = new DBManager();
			
			artikel = dbm.artikelSuchen(suche, kategorie);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		for (String sArtikel : artikel) {
			String[] artID = sArtikel.split("\t", 2) ;
			artikelIDs.add(artID[0]);
			
		}
		
		
		
		
		if (!(artikel.get(0).equals("Die Suche ergab leider keine Treffer!"))) {
			out.println("<form action='bestellung.jsp'> <br/> \n");
			out.println("[]	Artikelnummer	Preis	Bezeichnung	Auf Lager	Beschreibung<br/>");
			for(String art : artikel)
			{
				int i = 0;
				out.println("<input type='checkbox' name='bestellt' value='"+ artikelIDs.get(i) +"'/>"
						+ art + "<br/><br/>");
				i++;
			}
			out.println("Bestaetigen sie ihre Bestellung: <input type='text' name='email' /> <br/>"
					+ "Passwort: <input type='text' name='password'/> <br/>");
			out.println( "<input type='submit' value='Bestellen'/>"
					+ "</form>");
					
		}
		else {
			out.println("<b>Die Suche ergab leider keine Treffer!</b>");
		}
		
		
		out.println("<footer><p id='pFooter'>\r\n" + 
				"Zuletzt aktuallisiert:" + (new java.util.Date().toString() )+ " \r\n " + 
				"</p></footer></body> </html>");
		
		
		



		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
